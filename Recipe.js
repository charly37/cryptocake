'use strict';

var log = require('./Logger.js').log;
var request = require('request');
//bittrex
var bittrex = require('node-bittrex-api');
//db
//mine
var Exchanges = require('./Exchanges.js');

/** This class represents a recipe (group of crypto currencies). */
class Recipe {

  /**
    * This function create a new Recipe.
    * @constructor
    * @param {String} iName - Name of recipe.
    * @param {String} iDescription - Description of recipe.
    * @param {String} iOwner - Creator of the recipe.
    * @param {Array} iCompo - Compo of the recipe.
    * @param {Object} iDataStore - DataStore interface.
    */
  constructor(iName, iDescription, iOwner, iCompo, iDataStore) {
    this.name = iName;
    this.description = iDescription;
    this.creator = "Charles";
    this.compo = iCompo;
    this.human = true;
    this.lastUpdateTime = undefined;
    this.owner = iOwner;


    //datastore info
    this._dataStore = iDataStore
  }

  /**
  * TODO Evaluate the performance of a recipe.
  */
  evaluatePerf() {
    let aExchanges = new Exchanges();
    var aCoinMarketCapFakeExchange = Exchanges.getExchangeByName("aCoinMarketCap");
    var aFakeCake = this.cookk(1000, aCoinMarketCapFakeExchange, [])

  }

  /**
  * This function cook a recipe to provide amount of token to buy depending on the amount to invest.
  * @param {number} aAmountToInvest - Amount to invest.
  * @param {string} iExchange - Exchange to use.
  * @param {Array} iUserHolding - Array of token hold by the user.
  */
  cookk(aAmountToInvest, iExchange, iUserHolding) {
    var aCake = { receipe: this.name, compo: [] };
    var aComposition = this.compo;

    aComposition.forEach(function (value) {
      log.info("Evaluating ", value.token);
      var aToken = value;
      var aAmountToBuy = value.percent * aAmountToInvest / 100;
      aToken["amountToBuy"] = aAmountToBuy
      //log.info("Amount to buy: ", aAmountToBuy);
      var aValueOfCurrency = iExchange.getMarket(value.token);
      log.info("Value of currency: ", aValueOfCurrency.value);
      var aQuantityToBuy = aAmountToBuy / aValueOfCurrency.value;
      aToken["quantityToBuy"] = aQuantityToBuy
      //User holding for this token ?
      var aUserTokenHolding = iUserHolding.filter(function suffisammentGrand(element) {
        //log.info("filtering ", element);
        return element.Currency == value.token;
      });
      //log.info("Interesect ", aUserTokenHolding);
      if (aUserTokenHolding.length == 1) {
        log.info("User hold some of these token ", aUserTokenHolding);
        aToken["existingAmount"] = aUserTokenHolding[0].Balance;
      }
      else {
        aToken["existingAmount"] = "?";
      }
      aCake.compo.push(aToken);
      //log.info("Quantity to buy: ", aQuantityToBuy);
    });
    log.info("aCake ", aCake);
    return aCake;
  }


  save() {
    //TODO
    log.info('Going to save the recipe: ', this.name, ' with datastore', this._dataStore);
    var aRecipe = this
    var promise = new Promise(function (resolve, reject) {
      var aRecipeValue = {
        name: aRecipe.name,
        description: aRecipe.description,
        creator: aRecipe.creator,
        compo: aRecipe.compo,
        human: aRecipe.human,
        lastUpdateTime: aRecipe.lastUpdateTime,
        owner: aRecipe.owner
      };
      if (process.env.STORAGE == "MongoDB") {

        const collection = this._dataStore.collection('recipes');
        // Insert some documents
        collection.insertOne(
          aRecipeValue
          , function (err, result) {
            if (!err) {
              log.info('Recipe properly saved in Recipe');
              resolve("Stuff worked!");
            }
            else {
              log.error('Error when saving the Recipe: ', err);
              reject(Error("It broke"));
            }
          });

      }
      else {
        // do a thing, possibly async, then…
        // Save data to Datastore. 

        var aRecipeKey = aRecipe._dataStore.key('Recipes');
        aRecipe._dataStore.save({
          key: aRecipeKey,
          data: aRecipeValue
        }, function (err) {
          if (!err) {
            log.info('Recipe properly saved in Recipe');
            resolve("Stuff worked!");
          }
          else {
            log.error('Error when saving the Recipe: ', err);
            reject(Error("It broke"));
          }

        });


      }
    }.bind(this));
    return promise;
  }

  /**
  * This function cook a recipe to provide amount of token to buy depending on the amount to invest.
  * @param {number} aAmountToInvest - Amount to invest.
  * @param {string} iExchange - Exchange to use.
  * @param {string} iKey - API Key to connect to the exchange to get user holding.
  * @param {string} iSecret - API Secret to connect to the exchange to get user holding.
  */
  cook(aAmountToInvest, iExchange, iKey, iSecret) {
    var promise = new Promise(function (resolve, reject) {
      if (iKey === "undefined") {
        log.info("No key provided");
        var aCake = this.cookk(aAmountToInvest, iExchange, []);
        log.info("No key aCake", aCake);
        resolve(aCake);
      }
      else {
        log.info("key provided");
        bittrex.options({
          'apikey': iKey,
          'apisecret': iSecret,
        });
        bittrex.getbalances(function (data, err) {
          if (err) {
            return log.error(err);
            reject(Error("It broke"));
          }
          log.info("Data retrieved from the user account on bittrex: ", data);
          var aUserHolding = data.result;
          //compare that to recipe
          var aCake = this.cookk(aAmountToInvest, iExchange, aUserHolding);


          log.info("aCake ", aCake);
          if (true) {
            log.info('resolve');
            resolve(aCake);
          }
          else {
            reject(Error("It broke"));
          }
        }.bind(this));

      }

    }.bind(this));

    log.info("leaving cook ");
    return promise;
  }

  /**
  * This function update the recipe composition according to the recipe.
  */
  update() {
    log.info("Updating recipe ", this.name);
    var promise = new Promise(function (resolve, reject) {
      resolve("OK");
    });
    return promise;
  }

};

module.exports = Recipe;