# Test application

This application is a test to allow me to buy bundle of crypto currencies to have a more diverse allocation and thus reduce risks.


# Fonctional process

The site offers several "recipes" that can be used to buy bundle of crypt currencies. 
A recipe is a bundle of currencies. It can be either maintened by someone or buy an automatic process. 
The idea is pretty similar to mutual fund and Tracker that are available for the stock market.

## How to use it

You need first to enter 
- amount he want to invest
- exchange he usually use to buy/sell (for now it is only use to retrieve the amout you have - and only for bittrex). It require the user to enter the key/secret of its API access.
- recipe he want to use.

The exchange and API key/secret are OPTIONAL. It is only use to retrieve the amount you have on bittrex and compare with what you should had according to the recipe you choose.
Be very carrful with your API key/secret. You should not trust this website and only use READ ONLY key (I would also suggest you to change your key after just to be safe).

Once you entered all info click on the "cook" button to get the amount of tokens you should buy (it will also show how much you have if you enter your key info for bittrex)

# Technical process

Based on a nodejs Server with Express for the Backend and offering REACT for the frontend. 
Data can be store on mongoDb or Google Cloud Datastore (I only quickly tested Mongo with mlab offer).

The server use JSDOC to generate its documentation http://usejsdoc.org/ 
```
./node_modules/.bin/jsdoc . -d Documentation/Server
```

To run the server:
```
export STORAGE=MongoDB
export MONGODB_URL=mongodb://....
node Main.js
```

# TODO
It is just a quick test and probably badly designed with lot of bugs but it usually work enough for me... There are lot of stuff to improve ;)