'use strict';

var log = require('./Logger.js').log;
var Recipe = require('./Recipe.js');
var request = require('request');

/** This class represents your recipes. */
class Recipes {

  /**
    * This function create a new Recipes.
    * @constructor
    * @param {Object} iDataStore - DataStore interface.
    */
  constructor(iDataStore) {
    this.recipes = [];
    this._dataStore = iDataStore

    //some hardcode for test
    let aFullBtcRecipe = new Recipe("Full BTC", "Fully bullish on BTC.", "rand@rand.com", [{ token: "BTC", percent: 100 }], this._dataStore);
    let aHistoricalRecipe = new Recipe("Historical", "The oldest ones.", "rand2@rand2.com", [{ token: "BTC", percent: 50 }, { token: "ETH", percent: 50 }], this._dataStore);
    let aCoinbaseRecipe = new Recipe("Coinbase", "The one available on coinbase.", "Robotic Receipe", [{ token: "BTC", percent: 33 }, { token: "ETH", percent: 33 }, { token: "LTC", percent: 33 }], this._dataStore);
    let aTop5CoinMarketCap = new Recipe("Top5CoinMarketCap", "The Top 5 crypto by Market cap (based on CoinMarketCap data).", "Robotic recipe", [], this._dataStore);
    aTop5CoinMarketCap.update = function () {
      //https://bittrex.com/api/v1.1/public/getticker?market=USDT-BTC
      log.info("Updating recipe aTop5CoinMarketCap");
      var promise = new Promise(function (resolve, reject) {
        var options = {
          method: 'GET',
          url: 'https://api.coinmarketcap.com/v1/ticker/?limit=5',
          headers: {}
        };

        request(options, function (error, response, body) {
          if (error) {
            log.error(error);
            throw new Error(error);
          }
          var aJsonBody = JSON.parse(body);
          log.info(aJsonBody);
          var aTotalMarketCap = aJsonBody.reduce(function (acc, aTokenObj) {
            //log.info("acc: ", acc, "__aTokenObj: ", aTokenObj);
            return acc + parseFloat(aTokenObj.market_cap_usd)
          }, 0.0);
          log.info("aTotalMarketCap: ", aTotalMarketCap, "END");
          //https://stackoverflow.com/questions/5732043/javascript-reduce-on-array-of-objects
          var aFilteredResponse = aJsonBody.map(function (aOneToken) {
            return { token: aOneToken.symbol, percent: Number((100 * aOneToken.market_cap_usd / aTotalMarketCap).toFixed(2)) };
          });
          log.info("aFilteredResponse: ", aFilteredResponse);
          this.compo = aFilteredResponse;
          log.info("this: ", this);
          resolve("Stuff worked!");
        }.bind(this));
      }.bind(this));
      return promise;
    };

    this.recipes.push(aFullBtcRecipe);
    this.recipes.push(aHistoricalRecipe);
    this.recipes.push(aCoinbaseRecipe);
    //run a manual update to have the compo member properly populated
    //aTop5CoinMarketCap.update();
    this.recipes.push(aTop5CoinMarketCap);
    //this.getFromStorage();
  }

  getRecipes(){
    return this.recipes.map(function(aOneRecipe){
      var aCleanRecipe={};
      aCleanRecipe.name = aOneRecipe.name;
      aCleanRecipe.description = aOneRecipe.description;
      aCleanRecipe.creator = aOneRecipe.creator;
      aCleanRecipe.compo = aOneRecipe.compo;
      aCleanRecipe.human = aOneRecipe.human;
      aCleanRecipe.lastUpdateTime = aOneRecipe.lastUpdateTime;
      return aCleanRecipe;
    });
  }

  getRecipeByName(aRecipeName) {
    var aRecipe = this.recipes.filter(aOneRecipe => aOneRecipe.name == aRecipeName);
    return aRecipe[0];
  }

  getFromStorage() {
    const collection = this._dataStore.collection('recipes');
    // Insert some documents
    collection.find().toArray(function (err, aRecipes) {
      if (err) throw err;
      log.info(aRecipes);
      //this.recipes = aRecipes;
    }.bind(this));
  }

  updates() {
    log.info("Updating recipes");
    this.recipes.forEach(function (aOneRecipe) {
      aOneRecipe.update()
      .then(function(){
        //aOneRecipe.save()
        //.catch(function (err) {
        //  log.error('Failure during recipe backup: ', err);
        //});
      });
      
    });
  }

};

module.exports = Recipes;