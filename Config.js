module.exports = {
  facebookAuth: {
    clientID: process.env.FacebookId,
    clientSecret: process.env.FacebookSecret,
    callbackURL: 'http://djynet.xyz/auth/facebook/callback',
  },
  googleAuth: {
    clientID: process.env.GoogleId,
    clientSecret: process.env.GoogleSecret,
    callbackURL: 'http://localhost:3000/auth/google/callback',
  },
};
