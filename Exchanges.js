'use strict';

var log = require('./Logger.js').log;
var Exchange = require('./Exchange.js');
var request = require('request');

/** This class represents your list of exchanges. */
class Exchanges {

  /**
    * This function create a new Exchanges.
    * @constructor
    */
  constructor() {
    this.exchanges = [];

    //hardcode bittrex for tests
    let aBittrex = new Exchange("bittrex");
    aBittrex.market = [{ asset: "BTC", bittrexTicker: "USDT-BTC", value: 1 }, { asset: "ETH", bittrexTicker: "USDT-ETH", value: 1 }, { asset: "LTC", bittrexTicker: "USDT-LTC", value: 1 }, { asset: "XRP", bittrexTicker: "USDT-XRP", value: 1 }, { asset: "BCH", bittrexTicker: "USDT-BCC", value: 1 }, { asset: "ADA", bittrexTicker: "USDT-ADA", value: 1 }];
    this.exchanges.push(aBittrex);
    //trick to be able to evaluate a recipe. We use coinmarketcap as an exchange 
    let aCoinMarketCap = new Exchange("coinMarketCap");
    aCoinMarketCap.market = [];
    aCoinMarketCap.update = function () {
      var options = {
        method: 'GET',
        url: 'https://api.coinmarketcap.com/v1/ticker/?limit=50',
        headers: {}
      };
      request(options, function (error, response, body) {
        if (error) {
          log.error(error);
          throw new Error(error);
        }
        var aJsonBody = JSON.parse(body);
        //log.info(aJsonBody);
        //https://stackoverflow.com/questions/5732043/javascript-reduce-on-array-of-objects
        aJsonBody.forEach(function (aOneToken) {
          //console.log(element);
          this.market.push({asset:aOneToken.symbol, bittrexTicker: "USDT-BTC", value: aOneToken.price_usd});
        }.bind(this));
      }.bind(this));
    };
    this.exchanges.push(aCoinMarketCap);
  }

  getExchangeByName(aExchangeName) {
    var aExchange = this.exchanges.filter(aOneExchange => aOneExchange.name == aExchangeName);
    return aExchange[0];
  }

  updates() {
    log.info("Updating exchanges");
    this.exchanges.forEach(function (aOneExchange) {
      aOneExchange.update();
    });
  }

};

module.exports = Exchanges;