FROM centos:7
MAINTAINER charles.walker.37@gmailc.om

RUN  yum -y install epel-release
RUN  yum -y install nodejs

ADD . /var/www

RUN cd /var/www && npm install && cd /var/www/fereact && npm install && npm run build

EXPOSE 8777

CMD ["/usr/bin/node", "/var/www/Main.js"]