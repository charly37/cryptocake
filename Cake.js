'use strict';

var log = require('./Logger.js').log;

/** This class represents a Cake (result of a recipe inflated with an amount to invest). */
class Cake {

  /**
    * This function create a new cake.
    * @constructor
    */
  constructor() {
    this.name = "unknow";
    this.description = "unknow";
    this.compo = [];
  }

  /**
  * This function is useless.
  */
  getSplit(aRecipe, aAmountToInvest) {
    var aCake = { receipe: aRecipe.name, compo: aRecipe.compo };
    aCake.compo.forEach(function (value) {
      log.info("Evaluating ", value.token);
      var aAmountToBuy = value.percent * aAmountToInvest / 100;
      value["amountToBuy"] = aAmountToBuy
      //log.info("Amount to buy: ", aAmountToBuy);
      var aQuantityToBuy = aAmountToBuy / aValues[value.token];
      value["quantityToBuy"] = aQuantityToBuy
      //log.info("Quantity to buy: ", aQuantityToBuy);
    });
    log.info("aCake ", aCake);
    return aCake;
  }

};

module.exports = Cake;