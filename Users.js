'use strict';

var log = require('./Logger.js').log;
var User = require('./User.js');
var request = require('request');

/** This class represents your Users. */
class Users {

  /**
    * This function create a new Users.
    * @constructor
    * @param {Object} iDataStore - DataStore interface.
    */
  constructor(iDataStore) {
    this.Users = [];
    this._dataStore = iDataStore
  }

  getUserByName(aUserName) {
    var aUser = this.Users.filter(aOneUser => aOneUser.name == aUserName);
    return aUser[0];
  }

  getNewUser(){
    var aUser;
    if (this._dataStoreStr == "MongoDB") {
      aUser = new User();
    }
    else {
      var aUser = new User(this._dataStore);
      aUser = aUser;
    }

    aUser._name = "Name";
    return aUser;
  }

  findOne(iUserId) {
    var me = this
    //var aUuid=uuid();
    var promise = new Promise(function (resolve, reject) {
      // do a thing, possibly async, then…
      // Save data to Datastore. 
      if (process.env.STORAGE == "MongoDB") {
        log.info('Going to querry MongoDB DataStore Service');

        const collection = this._dataStore.collection('users');
        // Insert some documents
        collection.find({ '_userId': iUserId }).toArray(function (err, aUsers) {
          if (err){
            log.info('User not found');
            resolve(null);
          } 
          log.info('User found: ',aUsers);
          if (aUsers.length==1){
            resolve(aUsers);
          }
          else{
            resolve(null);
          }
          
        }.bind(this));
      }
      else {
        log.info('Going to querry Google DataStore Service for users with uuid: ');
        var query = me._dataStore.createQuery('Users');
        var caliQuery = query.filter('_employeeID', '=', iUserId);
        var aReturnPromise = me._dataStore.runQuery(caliQuery)
          .then((results) => { 
            //Task entities found.
            log.info('findOne Google DataStore Over', results, 'for uuid: ');
            const tasks = results[0];
            if (tasks.length>0) {
              log.info('resolve');
              resolve(tasks[0]);
            }
            else {
              //reject(Error("It broke"));
              log.info('We did not find the user in GDS')
              resolve(null)
            }
          })
          .catch(function (err) {
            log.error('Error with Going to querry Google DataStore Service. Error detail: ', err, ' for uuid: ');
            reject(Error('Error with Going to querry Google DataStore Service. Error detail: ', err));
          });
      }
    }.bind(this));
    return promise;
  }
};

module.exports = Users;