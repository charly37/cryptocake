'use strict';

///////////////////////////
//Initialize Dependancies//
///////////////////////////

//Used for logging nad more (clean json response circular dep)
const util = require('util')

//Express framework
var express = require("express");
var app = express();

//To serve the static files
var path = require("path");

//For stack reresh
var cron = require('node-cron');

//For session
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var expressSession = require('express-session');

//HTTPS
var url = require('url');
var fs = require('fs');
var https = require('https');

//db
const assert = require('assert');
const MongoClient = require('mongodb').MongoClient;
//swagger
var swaggerTools = require('swagger-tools');

//Passport - For authentification
var passport = require('passport');
//Passport - Strategies for authentification
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
//var FacebookStrategy = require('passport-facebook').Strategy;
//
//bittrex
var bittrex = require('node-bittrex-api');

//Our modules
var Recipes = require('./Recipes.js');
var Users = require('./Users.js');
var Exchanges = require('./Exchanges.js');
var Recipe = require('./Recipe.js');
var Exchange = require('./Exchange.js');
var log = require('./Logger.js').log;
//Config
var Config = require ('./Config.js');
//////////////////////
//Initialize the APP//
//////////////////////

//Express - for session
app.use(cookieParser());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(expressSession({
  secret: 'somesecrettokenhere',
  resave: true,
  saveUninitialized: true

}));

//Passport - For authentification
app.use(passport.initialize());

//Passport - For session
app.use(passport.session());

//Used to serve the static files like the images/sounds otherwise the brwoser will fails when loading images/sounds
app.use('/', express.static(__dirname + '/fereact/build'));
//http://stackoverflow.com/questions/21335868/how-to-protect-static-folder-in-express-with-passport
app.use('/node_modules', express.static(__dirname + '/node_modules'));

// swaggerRouter configuration
var options = {
  controllers: './controllers',
  useStubs: process.env.NODE_ENV === 'development' ? true : false // Conditionally turn on stubs (mock mode)
};



var request = require('request');
//require('request-debug')(request);


/**
 * This function is a MW layer used to ensure that the user is logged. If not it will redirect the user to the login page.
 */
function authenticationMiddleware() {
  return function (req, res, next) {
    if (req.isAuthenticated()) {
      return next()
    }
    res.redirect('/loginEntry')
  }
}

/**
 * This function is call by PassportJS MW to serlialize the user session in cookies(because everybody like cookies).
 */
passport.serializeUser(function (user, done) {
  done(null, user._employeeID);
});

/**
 * This function is call by PassportJS MW to retrieve the user session from information store in cookies.
 */
passport.deserializeUser(function (id, done) {
  log.info("Entering deserializeUser with id: ", id);
  aUsers.findOne(id)
    .then(function (user) {
      log.info("Promise match. User: ", user);
      if (user) {
        log.info("User is not null: ");
        return done(null, user);
      } else {
        log.error('No user found when trying to deserializing');
        //return done(err);
      }
    })
    .catch(function (err) {
      log.error('Error with the promise when deserializing user. Error detail: ', err);
    });
});

var aMongoDb;
var openDbConnection = function(){
  var promise = new Promise(function (resolve, reject) {
    log.info('Selected storage type: ', process.env.STORAGE);
    
    if (process.env.STORAGE == "MongoDB") {
      log.info('Connecting to MongoDB');
      //Parameters to connect to DB
      const aMongoUrl = process.env.MONGODB_URI;
      log.info('aMongoUrl: ', aMongoUrl);
      // Database Name
      const dbName = 'charly37';
      // Use connect method to connect to the server
      MongoClient.connect(aMongoUrl, function (err, client) {
        assert.equal(null, err);
        log.info("Connected successfully to server");
        aMongoDb = client.db(dbName);
        resolve("Stuff worked!");
        //client.close();
      });
    }
    else {
      //Storage
      var aGcpProjectName = 'r-box-devops'
      var datastore = require('@google-cloud/datastore')({
        projectId: aGcpProjectName,
        keyFilename: '/SAD/r-box-devops'
      });
    }
  });
  return promise;
}



//Store all the receipes
let aRecipes;
let aUsers;
//Store all the exchanges
let aExchanges = new Exchanges();

var initinitServer = function () {
  log.info("Start the initialization of the server")
  log.info("Going to open the DB connection")
  openDbConnection()
  .then(function(){
    log.info("DB connection open. Now refreshing data")
    aRecipes = new Recipes(aMongoDb);
    aUsers = new Users(aMongoDb);
    initServer();
  })
  
}

/**
 * To populate data used by the server.
 */
var initServer = function () {
  log.info("Refresh")
  aRecipes.updates();
  aExchanges.updates();
}

//Google Strategy
passport.use(new GoogleStrategy({
  clientID: Config.googleAuth.clientID,
  clientSecret: Config.googleAuth.clientSecret,
  callbackURL: Config.googleAuth.callbackURL,
},
function(token, refreshToken, profile, done) {
  log.info('entering google auth');
  process.nextTick(function() {

    var aId = "GCP" + profile.id;
    log.info('Locking for google user with id: ',aId);

    aUsers.findOne(aId)
    .then(function (user) {
      log.info("Promise match. User: ", user);
      if (user) {
        log.info("User is not null: ");
        return done(null, user);
      } else {
        log.info('Creating new user');
        var aNewUser = aUsers.getNewUser();
        aNewUser._employeeID = aId;
        return done(null, aNewUser);
        /*var aP = aNewUser.save()
        aP.then(function () {
          log.info('Promise of user saving resolve in Server');
          return done(null, aNewUser);
        })
        .catch(function (err) {
          log.error('Error when saving the user in server. Error detail: ', err);
        });*/
      }
    })
    .catch(function (err) {
      log.error('Error with the promise when looking for passport user. Error detail: ', err);
      return done('Error with the promise when looking for passport user. Error detail: ', err);          
    });

  });
}));

// Google routes
app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

app.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/',
  failureRedirect: '/login'
}));

/**
 * Route to get the list of cakes (result of recipes + amount to invest).
 */
app.get('/cake', function (req, res) {
  log.info('Entering GET for route /cake');
  var aReceipeToCook = req.query.recipe;
  var aAmountToInvest = req.query.amount;
  var aExchangeToUse = req.query.exchange;
  var aUserKey = req.query.readonlykey;
  var aUserSecret = req.query.readonlysecret;
  log.info('Cooking cake with amount ', aAmountToInvest, " and receipe ", aReceipeToCook, " and exchange ", aExchangeToUse, " and user key ", aUserKey, " and user secret ", aUserSecret);
  var aRecipeToCook = aRecipes.getRecipeByName(aReceipeToCook);
  log.info('Recipe retrieved ', aRecipeToCook);
  var aExchange = aExchanges.getExchangeByName(aExchangeToUse);
  log.info('Exchange retrieved ', aExchange);
  var aCake = aRecipeToCook.cook(aAmountToInvest, aExchange, aUserKey, aUserSecret)
    .then(function (aCacke) {
      log.info("Promise match. Cacke: ", aCacke);
      res.json(aCacke);
    })
    .catch();

});

/**
 * Route to get the list of existing recipes.
 */
app.get('/recipes', function (req, res) {
  log.info('Entering GET for route /recipes');
  log.info('recipes are ', aRecipes.getRecipes());
  res.json(aRecipes.getRecipes());
});

/**
 * Route to get the list of existing exchanges.
 */
app.get('/exchanges', function (req, res) {
  log.info('Entering GET for route /exchanges');
  res.json(aExchanges.exchanges);
});


//Initial server setup to retieve all needed data.
initinitServer();
//Then refresh data every 6 minutes
cron.schedule('*/60 * * * *', function () {
  log.info('Refreshing info');
  initServer();
});

app.listen(8777);