'use strict';

var log = require('./Logger.js').log;
var request = require('request');

/** This class represents an echange of crypto currencies. */
class Exchange {

    /**
    * This function create a new Exchange.
    * @constructor
    * @param {String} iName - Name of the exchange.
    */
    constructor(iName) {
        this.name = iName;
        this.market = [];
        //[{ asset: "BTC", bittrexTicker: "USDT-BTC" , value:1}, { asset: "ETH", bittrexTicker: "USDT-ETH", value:1 }, { asset: "LTC", bittrexTicker: "USDT-LTC", value:1 }, { asset: "XRP", bittrexTicker: "USDT-XRP" , value:1}, { asset: "BCH", bittrexTicker: "USDT-BCC" ,value:1}, { asset: "ADA", bittrexTicker: "USDT-ADA" , value:1}, { asset: "MIOTA", bittrexTicker: "USDT-MIOTA" , value:1}, { asset: "XEM", bittrexTicker: "USDT-XEM" , value:1}, { asset: "DASH", bittrexTicker: "USDT-DASH" , value:1}, { asset: "XLM", bittrexTicker: "USDT-XLM" , value:1}]
    }

    /**
    * This function send the USD value of one token.
    * @param {String} iMarketName - Name of token you want the value for.
    */
    getMarket(iMarketName){
        log.info("Entering getMarket for ", iMarketName);
        var aMarket = this.market.filter(aOneMarket => aOneMarket.asset == iMarketName);
        log.info("result ", aMarket);
        return aMarket[0];
    }

    /**
    * This function update the info about token from the exchange.
    */
    update() {
        this.market.forEach(function (aAsset) {
            log.info("Updating ", aAsset.asset);
            //log.info("Quantity to buy: ", aQuantityToBuy);
            this.updateOneAsset(aAsset);
        }.bind(this));
    }

    updateOneAsset(iAsset) {
        log.info("Updating iAsset ", iAsset);

        //https://bittrex.com/api/v1.1/public/getticker?market=USDT-BTC
        var options = {
            method: 'GET',
            url: 'https://bittrex.com/api/v1.1/public/getticker',
            qs: { market: iAsset.bittrexTicker },
            headers: {}
        };

        request(options, function (error, response, body) {
            if (error) {
                log.error("Error when calling bittrex for value of ", iAsset.bittrexTicker)
                throw new Error(error);
            }
            var aJsonBody = JSON.parse(body);
            //log.debug(aJsonBody);
            if (!aJsonBody.success) {
                log.error("Error when extracting value of ", iAsset.bittrexTicker, " from bittrex response")
            }
            else {
                iAsset.value = aJsonBody.result.Last;
                //ioMarket["valueUsd"] = aJsonBody.result.Last;
            }
        });

    }


};

module.exports = Exchange;