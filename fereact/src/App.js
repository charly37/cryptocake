import React, { Component } from 'react';
//react router maybe
//import { Link } from 'react-router-dom'
import { Route, Link, NavLink, HashRouter } from "react-router-dom";
import './App.css';
//For material UI
import { render } from 'react-dom';
// button
import Button from 'material-ui/Button';
//text field
import TextField from 'material-ui/TextField';
//table
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
//selects
import Select from 'material-ui/Select';
//app bar
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
//menu in app bar
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import Menu, { MenuItem } from 'material-ui/Menu';
//charts
//pie
import { PieChart, Pie, Legend, Tooltip } from 'recharts';
//line
import { LineChart, Line, CartesianGrid } from 'recharts';




class App extends Component {
  constructor(props) {
    super(props);
    this.state = { recipes: [], anchorEl: null, aAmountToInvest: 2020, aRecipeToUse2: "Coinbase", aSelected: "toto", aExchange: "bittrex", aReadOnlyKey: undefined, aReadOnlySecret: undefined, aCake: { compo: [] }, aColor: { 'BTC': '#F2A900', 'ETH': '#3D3E3F', 'LTC': '#989898', 'XRP': '#1886BD', 'BCH': '#009900', 'ADA': '#2E2E2E', 'DASH': '#1C75BC', 'XLM': '#0EB8E8', 'XEM': '#2CBAAD', 'MIOTA': '#000000' }, value: 1, }
    //https://stackoverflow.com/questions/39176248/react-js-cant-read-property-of-undefined
    this.handleClick = this.handleClick.bind(this); //can be avoid ES6 =>
    this.handleChangeAmountInvestTextFiled = this.handleChangeAmountInvestTextFiled.bind(this);
    this.handleChangeDropDownExchenge = this.handleChangeDropDownExchenge.bind(this);
    this.handleChangeReadOnlyKeyTextFiled = this.handleChangeReadOnlyKeyTextFiled.bind(this);
    this.handleChangeReadOnlySecretTextFiled = this.handleChangeReadOnlySecretTextFiled.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleMenu = this.handleMenu.bind(this);
    this.remap = this.remap.bind(this);
    this.aRecipeToUse1 = "Coinbase";
    this.handleCellClick = this.handleCellClick.bind(this);


  }


  handleMenu(event) {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose() {
    this.setState({ anchorEl: null });
  };

  handleChangeAmountInvestTextFiled(event) {
    //console.log('Entering handleChangeAmountInvestTextFiled with event: ', event);
    this.setState({ aAmountToInvest: event.target.value });
  }

  handleChangeReadOnlyKeyTextFiled(event) {
    //console.log('Entering handleChangeReadOnlyKeyTextFiled with event: ', event);
    this.setState({ aReadOnlyKey: event.target.value });
  }

  handleChangeReadOnlySecretTextFiled(event) {
    //console.log('Entering handleChangeReadOnlySecretTextFiled');
    this.setState({ aReadOnlySecret: event.target.value });
  }

  handleChangeDropDownExchenge(event, index, value) {
    //console.log('Entering handleChangeDropDownExchenge');
    //this.setState({ value });
  }

  handleClick() {
    console.log('click with amount ', this.state.aAmountToInvest, ' and recipe1 ', this.aRecipeToUse1, ' and recipe2 ', this.state.aRecipeToUse2, ' and exchange ', this.state.aExchange, ' and key ', this.state.aReadOnlyKey, ' and secret ', this.state.aReadOnlySecret);
    var params = {
      amount: this.state.aAmountToInvest,
      //recipe: this.aRecipeToUse1,
      recipe: this.state.aRecipeToUse2,
      exchange: this.state.aExchange,
      readonlykey: this.state.aReadOnlyKey,
      readonlysecret: this.state.aReadOnlySecret
    }

    var esc = encodeURIComponent;
    var query = Object.keys(params)
      .map(k => esc(k) + '=' + esc(params[k]))
      .join('&');

    fetch('/cake?' + query)
      .then(res => res.json())
      .then(aCake => this.setState({ aCake }));
  }

  handleClickLogin() {
    console.log('Click login ');
    var params = {
      amount: "tt"

    }

    var esc = encodeURIComponent;
    var query = Object.keys(params)
      .map(k => esc(k) + '=' + esc(params[k]))
      .join('&');

    fetch('/auth/google');
  }

  handleCellClick(event) {
    console.log('handleSelect with event ', event.target.dataset.myRowIdentifier);
    this.aRecipeToUse1 = event.target.dataset.myRowIdentifier;
    this.setState({ aRecipeToUse2: event.target.dataset.myRowIdentifier });
    console.log('aRecipeToUse1', this.aRecipeToUse1);
    console.log('aRecipeToUse2', this.state.aRecipeToUse2);
  }

  remap(aOneSubToken) {
    var aNewSubToken = { 'name': aOneSubToken.token, 'value': aOneSubToken.percent, 'fill': this.state.aColor[aOneSubToken.token] };
    return aNewSubToken;
  }

  componentDidMount() {
    fetch('/recipes')
      .then(res => res.json())
      .then(recipes => this.setState({ recipes }))
      .catch(function (error) {
        console.log("error when fetching recipes", error);
      });
  }

  render() {
    return (

      <div>
        <AppBar position="static">
          <Toolbar>
            <IconButton color="contrast" aria-label="Menu" aria-owns={Boolean(this.state.anchorEl) ? 'menu-appbar' : null}
              aria-haspopup="true"
              onClick={this.handleMenu}>
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={this.state.anchorEl}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(this.state.anchorEl)}
              onClose={this.handleClose}
            >
              <MenuItem onClick={this.handleClose}>Home</MenuItem>
              <a href="/auth/google" style={{ textDecoration: 'none' }}><MenuItem onClick={this.handleClose}>test</MenuItem></a>
              <a target="_blank" href="https://bitbucket.org/charly37/cryptocake" style={{ textDecoration: 'none' }}><MenuItem onClick={this.handleClose}>Sources</MenuItem></a>
            </Menu>
            <Typography style={{ flex:1 }} type="title" color="inherit" >
              CryptoTest
            </Typography>
            <Button color="contrast" onClick={this.handleClickLogin}>Login</Button>
          </Toolbar>
        </AppBar>


        <h2>Choose the amount to invest</h2>
        <TextField id="amountToInvest" helperText="Amount to invest ($)" onChange={this.handleChangeAmountInvestTextFiled} /><br />
        <h2>Choose the exchange to use</h2>
        <Select
          //floatingLabelText="Frequency"
          value={this.state.value}
          onChange={this.handleChangeDropDownExchenge}
        >
          <MenuItem value={1}>Bittrex (USDT as medium)</MenuItem>
        </Select>
        <h2>Optional - Enter your read only key/secret (to compare the recipe with your holdings on exchange)</h2>
        <TextField id="2" helperText="readonly key" fullWidth onChange={this.handleChangeReadOnlyKeyTextFiled} /><br />
        <TextField id="3" helperText="readonly secret" fullWidth onChange={this.handleChangeReadOnlySecretTextFiled} /><br />
        <h2>Choose the receipe to use</h2>

        <Table>
          <TableHead>
            <TableRow>
              <TableCell>name</TableCell>
              <TableCell>owner</TableCell>
              <TableCell>description</TableCell>
              <TableCell>compo</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.recipes.map(recipe =>
              <TableRow hover key={recipe.name} onClick={event => this.handleCellClick(event)} selected={recipe.name === this.state.aRecipeToUse2} >
                <TableCell data-my-row-identifier={recipe.name}>{recipe.name}</TableCell>
                <TableCell data-my-row-identifier={recipe.name}>{recipe.owner}</TableCell>
                <TableCell data-my-row-identifier={recipe.name}>{recipe.description}</TableCell>
                <TableCell data-my-row-identifier={recipe.name}>
                  <PieChart width={350} height={350}>
                    <Pie data={recipe.compo.map(this.remap)} dataKey="value" nameKey="name" outerRadius={100} fill="#8884d8" label />
                    <Tooltip />
                    <Legend />

                  </PieChart>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>

        <Button raised onClick={this.handleClick}>
          Cook
      </Button>
        <h2>Result</h2>

        <Table>
          <TableHead >
            <TableRow>
              <TableCell>token</TableCell>
              <TableCell>Percentage</TableCell>
              <TableCell>Amont to buy $</TableCell>
              <TableCell>Quanitity to buy</TableCell>
              <TableCell>Quanitity you own</TableCell>
            </TableRow>
          </TableHead>
          <TableBody >
            {this.state.aCake.compo.map(aOneToken =>
              <TableRow key={aOneToken.token} >
                <TableCell>{aOneToken.token}</TableCell>
                <TableCell>{aOneToken.percent}</TableCell>
                <TableCell>{aOneToken.amountToBuy}</TableCell>
                <TableCell>{aOneToken.quantityToBuy}</TableCell>
                <TableCell>{aOneToken.existingAmount}</TableCell>
              </TableRow>
            )}

          </TableBody>
        </Table>
      </div>

    );
  }
}

export default App;